# class by Kaley Wilson
# This class is able to train a simple DNN on a coordinate data set that is a 17 value array that comes from coordinates / values extracted from using theh 
# YOLO algorithm.

import tensorflow as tf
from tensorflow.keras.layers import Dense
from tensorflow.keras.models import Sequential
from tensorflow.python.keras.callbacks import EarlyStopping, ModelCheckpoint
from sklearn.model_selection import train_test_split
import numpy as np
import os
from numpy import asarray

#change these paths to save and retrieve data
save_path_model = r"C:\path\to\save"
load_label_path = r"C:\path\to\load"
load_data_path = r"C:\path\to\load"

model = Sequential()
model.add(Dense(512, activation = 'relu', input_shape = (17,)))
model.add(Dense(256, activation = 'relu'))
model.add(Dense(256, activation = 'relu'))
model.add(Dense(64, activation = 'relu'))
model.add(Dense(32, activation = 'relu'))
model.add(Dense(32, activation = 'relu'))
model.add(Dense(7, activation='softmax'))
model.compile(optimizer='adam', loss="sparse_categorical_crossentropy", metrics=['accuracy'])

checkpoint = ModelCheckpoint(save_path_model, monitor='val_accuracy', verbose=1, save_best_only=True, mode='max', save_weights_only=True)
early_stopping = EarlyStopping(patience=15, restore_best_weights=True)
callbacks_list = [checkpoint, early_stopping]


# Datasets
labelsload = np.load(load_label_path, allow_pickle='TRUE').item()
labels = []
labels.extend(labelsload.keys())

X_train_label, X_test_label = train_test_split(labels, test_size=.2, random_state=42)
X_train_label, X_val_label =train_test_split(X_train_label, test_size=.2, random_state=42)

train = []
for label in X_train_label:
    cd = np.load(os.path.join(load_data_path, '{}.npy'.format(label)))
    cd = asarray(cd)
    train.append(cd)
X_train = train.copy()

train = []
for label in X_val_label:
    cd = np.load(os.path.join(load_data_path, '{}.npy'.format(label)))
    cd = asarray(cd)
    train.append(cd)
X_val = train.copy()
print("Done")

train = []
for label in X_test_label:
    cd = np.load(os.path.join(load_data_path, '{}.npy'.format(label)))
    cd = asarray(cd)
    train.append(cd)
X_test = train.copy()

X_train = asarray(X_train)
X_val = asarray(X_val)
X_test = asarray(X_test)

y_train = np.zeros(len(X_train_label))
i = 0
while i < len(X_train_label):
    y_train[i] = labelsload[X_train_label[i]]
    i+=1

y_val = np.zeros(len(X_val_label))
i = 0
while i < len(X_val_label):
    y_val[i] = labelsload[X_val_label[i]]
    i+=1

y_test = np.zeros(len(X_test_label))
i = 0
while i < len(X_test_label):
    y_test[i] = labelsload[X_test_label[i]]
    i+=1

# print(X_train)
# print(y_train)
model.fit(X_train, y_train, validation_data = (X_val, y_val), epochs=500, callbacks=callbacks_list)

model.save(save_path_model)

print(model.summary())
model.evaluate(X_test, y_test)