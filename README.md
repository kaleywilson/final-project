# Machine Learning && Tennis
---
This repository contains all files we created and used for this project. It includes image preprocessing, distributed training, a data generator, and the models we created and trained. Below are all the folders and explain
what each folder contains and how to use the files together.

## *img_processing*
This folder contains all the files needed to preprocess images.  
***image_extraction:***  
*'FrameExtraction'* is a file that is able to take video and label frame-by-frame using the keyboard to save images into the appropriate folders.  
  
***combine_images:***  
The three files in this folder are all used to create the combined images (player / racket images). Run the *'extract_person'* with the arguments described in the file. *'combine_images'* and *'yolo_extract'* are used within the program. Note that there are paths to change and also how to name the new saved images. Those are commented in the file.  
  
***full_imgResizer:***  
This file created the resized full images. Note that paths and how to name the saved images will need to be changed.  
  
***coordinate_yolo:***  
This file creates the coordinate data set. Note that the paths and how to name the images will need to be changed.  
  
***Other files:***  
*'yolo-coco'* is used for any programs that use YOLO. *'og_yolo'* is the original YOLO file that we based our own "programs" off of. *'loop_yolo'* is not that important but is able to run the YOLO algorithm on a full folder of images. 

## *DataGenerator*  
This folder contains *'data_labeling'* and *'datagenerator'* which involve saving the data into arrays and creating labels for each of the instances of data.  
***data_labeling:***  
This folder contains files to create numpy files and labels for all the data. Note that paths and names will need to be change.  

***datagenerator:***  
This folder contains the data generator in *'my_classes'*. *'keras_script'* trains a binary ResNet model, *'keras_script_softmax'* trains a multiclass ResNet model, and *'vgg16'* trains a VGG16 model and can easily change from a binary and multiclass classifer by changing the output layer. 
  
## *Coordinate Model*
This contains a file called **'model'** which simply is a model to take in the coordinate data. The coordinate data is created by the **'coordinate_yolo'** file in the **'img_processing'** folder.  
  
## *distributedTrainingBasic*  
This folder contains the necessary files to implement distributed training. 
***ResNetDistTrain:***  
This is an implementation of distributed training using the ResNet model.  
  
***VggDisTrain:***
This is an implementatino of distributed training using the ResNet model.  
  
***BasicDistTrain:***
An example of distributed training using a simple CNN with a MNIST data set. Link for more information: [Distributed Training](https://www.tensorflow.org/tutorials/distribute/multi_worker_with_keras)
  

