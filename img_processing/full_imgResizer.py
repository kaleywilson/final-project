import numpy as np
from numpy.core.defchararray import asarray
import tensorflow as tf
from tensorflow import keras
import os
import cv2
import os
from sklearn.model_selection import train_test_split
#assign image paths to variable
path1 = r"E:\TrainingSet_1"
Serve = r"E:\TrainingSet_1\Serve"
BackhandB = r"E:\TrainingSet_1\BackhandB"
BackhandV = r"E:\TrainingSet_1\BackhandV"
Dead = r"E:\TrainingSet_1\Dead"
ForehandB = r"E:\TrainingSet_1\ForehandB"
ForehandV = r"E:\TrainingSet_1\ForehandV"
OverheadV = r"E:\TrainingSet_1\OverheadV"
paths = [Serve,BackhandB, BackhandV, Dead, ForehandB, ForehandV, OverheadV]
data = np.zeros((180,320, 3))
labels = {}
serve_save = r"C:\Users\Study\Desktop\research-repository-1\data"
i=0
#iterate through paths to data path
for path in paths:
    print(path)
    for file in os.listdir(path):
        img = cv2.imread(os.path.join(path,file))
        # img = cv2.resize(img,(320,180))
        # img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        idimg = "id-{0}".format(i)
        # data[:,:,:] = img
        # np.save(os.path.join(serve_save, idimg), data)
        if path == Serve:
            labels[idimg] = 1
            print("s{0}".format(i))
        elif path == BackhandB:
            labels[idimg] = 0
            print("bb{0}".format(i))
        elif path == BackhandV:
            labels[idimg] = 0
            print("bv{0}".format(i))
        elif path == Dead:
            labels[idimg] = 0
            print("d{0}".format(i))
        elif path == ForehandB:
            labels[idimg] = 0
            print("fb{0}".format(i))
        elif path == ForehandV:
            labels[idimg] = 0
            print("fv{0}".format(i))
        elif path == OverheadV:
            labels[idimg] = 0
            print("ov{0}".format(i))
        i+=1
        
np.save("labels_bi.npy", labels)
