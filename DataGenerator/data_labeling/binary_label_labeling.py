# class by Kaley Wilson
# This class is to label a binary data set for serve or nonserve
import numpy as np
from scipy.sparse import data
import os
import cv2
import os

#create / change  necessary paths
data_path = r"C\path\to\data\to\label"
data_save = r"C:\path\to\save\data"


#initialize arrays to serve converted images
data = np.zeros((50,40, 3))
labels = {}




#iterate through paths to data folder
for folder in os.listdir(data_path):
    for file in os.listdir(folder):
        img = cv2.imread(os.path.join(folder,file))
        img = cv2.resize(img,(40,50))
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        data[:,:,:]=img
        np.save(os.path.join(data_save, os.path.splitext(file)[0]), data)
        if folder.endswith("non_serve"):
            labels[os.path.splitext(file)[0]] = 0
        else:
            labels[os.path.splitext(file)[0]] = 1

np.save(os.path.join(data_save, 'binary_labels'), labels)

