# class by Kaley Wilson
# This class is to label a binary data set for serve or nonserve

import numpy as np
import os
import cv2
import os

# create/change paths are necessary
path = r"C:\path\to\retrieve\data\with\folders"     #this is a path to a folder with all the labeled folders; use names found in the if statements below or change the below if statements
data_save = r"C:\path\to\save\data"
data = np.zeros((50,40, 3))
labels = {}

#iterate through paths to data folder
for folder in path:
    main_path = os.path.join(path, folder)
    for file in os.listdir(os.path.join(main_path)):
        img = cv2.imread(os.path.join(main_path,file))
        img = cv2.resize(img,(40,50))
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        data[:,:,:] = img
        print(file)
        if folder == "BackhandB":
            labels.update({os.path.splitext(file)[0]: 0})
            print("0")
        elif folder == "BackhandV":
            labels.update({os.path.splitext(file)[0]: 1})
            print("1")
        elif folder == "ForehandB":
            print("2")
            labels.update({os.path.splitext(file)[0]: 2})
        elif folder == "ForehandV":
            print("3")
            labels.update({os.path.splitext(file)[0]: 3})
        elif folder == "OverheadV":
            labels.update({os.path.splitext(file)[0]: 4})
        elif folder == "Serve":
            print("5")
            labels.update({os.path.splitext(file)[0]: 5})
        elif folder == "Dead":
            print("6")
            labels.update({os.path.splitext(file)[0]: 6})
        np.save(os.path.join(data_save, os.path.splitext(file)[0]), data)  
np.save("multiclass_labels.npy", labels)