import numpy as np
import os
import tensorflow as tf
from tensorflow import keras, Tensor
from tensorflow.keras.models import Sequential
from my_classes import DataGenerator
from sklearn.model_selection import train_test_split
from tensorflow.keras.layers import Input, Conv2D, ReLU, BatchNormalization,\
                                    Add, AveragePooling2D, Flatten, Dense, MaxPooling2D
from tensorflow.keras.models import Model
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping

# Parameters
params = {'dim': (50, 40),
          'batch_size': 32,
          'n_classes': 7,
          'n_channels': 3,
          'shuffle': True}

# Datasets
partition = {'train' : [], 'validation':[], 'test':[]}
labels = np.load("labels.npy", allow_pickle=True).item()

all_ids = []
all_ids.extend(labels.keys())
print(all_ids[0])


X_train, X_test = train_test_split(all_ids, test_size=0.2)
X_train, X_val = train_test_split(X_train, test_size=0.2)

partition.update({'train' : X_train})
partition.update({'validation':  X_val})
partition.update({'test' : X_test})

# Generators
training_generator = DataGenerator(partition['train'], labels, **params)
validation_generator = DataGenerator(partition['validation'], labels, **params)
test_generator = DataGenerator(partition['test'], labels, **params)

# Design model
def relu_bn(inputs: Tensor) -> Tensor:
    relu = ReLU()(inputs)
    bn = BatchNormalization()(relu)
    return bn

def residual_block(x: Tensor, downsample: bool, filters: int, kernel_size: int = 3) -> Tensor:
    y = Conv2D(kernel_size=kernel_size,
               strides= (1 if not downsample else 2),
               filters=filters,
               padding="same")(x)
    y = relu_bn(y)
    y = Conv2D(kernel_size=kernel_size,
               strides=1,
               filters=filters,
               padding="same")(y)

    if downsample:
        x = Conv2D(kernel_size=1,
                   strides=2,
                   filters=filters,
                   padding="same")(x)
    out = Add()([x, y])
    out = relu_bn(out)
    return out

def create_res_net():
    
    inputs = Input(shape=(40, 50, 3))
    num_filters = 64
    
    t = BatchNormalization()(inputs)
    t = Conv2D(kernel_size=3,
               strides=1,
               filters=num_filters,
               padding="same")(t)
    t = relu_bn(t)
    
    num_blocks_list = [2, 5, 5, 2]
    for i in range(len(num_blocks_list)):
        num_blocks = num_blocks_list[i]
        for j in range(num_blocks):
            t = residual_block(t, downsample=(j==0 and i!=0), filters=num_filters)
        num_filters *= 2
    
    t = MaxPooling2D(4)(t)
    t = Flatten()(t)
    outputs = Dense(7, activation='softmax')(t)
    
    model = Model(inputs, outputs)
    model.compile(
        optimizer='adam',
        loss='categorical_crossentropy',
        metrics=['accuracy']
    )

    return model

model = create_res_net()

#change this path to save the model
path = r'C:\save\model\path'
#add checkpoints and callbacks
checkpoint = ModelCheckpoint(os.path.join(path, "resnet_model.weights"), monitor='val_accuracy', verbose=1, save_best_only=True, mode='max', save_weights_only=True)
early_stopping = EarlyStopping(patience=5, restore_best_weights=True)
callbacks_list = [checkpoint, early_stopping]

# Train model on dataset
model.fit_generator(generator=training_generator,
                    validation_data=validation_generator, epochs=100, callbacks=callbacks_list)

#save model
model.save(os.path.join(path, "ResNet_Model"))

yhat = model.evaluate_generator(generator=test_generator)

print("Evaluation:")
print("loss: ", yhat[0])
print("accuracy: ", yhat[1])


