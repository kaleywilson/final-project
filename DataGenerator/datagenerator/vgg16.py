from tensorflow.keras.applications.vgg16 import VGG16
from sklearn.model_selection import train_test_split
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping
import numpy as np
from my_classes import DataGenerator
# from keras.models import Model
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Flatten
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.models import Model

# Parameters
params = {'dim': (50,40),
          'batch_size': 64,
          'n_classes': 7,
          'n_channels': 3,
          'shuffle': True}
# Datasets
partition = {}

labelsload = np.load("labels.npy",allow_pickle='TRUE').item()
labels = []
labels.extend(labelsload.keys())




X_train, X_test = train_test_split(labels, test_size=.2, random_state=42)
X_train, X_val =train_test_split(X_train, test_size=.2, random_state=42)

partition["train"]= np.asarray(X_train)
partition["validation"]= np.asarray(X_val)
partition["test"]= np.asarray(X_test)

# Generators
training_generator = DataGenerator(partition['train'], labelsload, **params)
validation_generator = DataGenerator(partition['validation'], labelsload, **params)
testing_generator = DataGenerator(partition['test'], labelsload, **params)

# load model without classifier layers
model = VGG16(include_top=False, input_shape=(50, 40, 3))
# mark loaded layers as not trainable
for layer in model.layers:
    layer.trainable = False
# add new classifier layers
flat1 = Flatten()(model.layers[-1].output)
class1 = Dense(1024, activation='relu')(flat1)
output = Dense(7, activation='softmax')(class1)
# define new model
model = Model(inputs=model.inputs, outputs=output)
model.compile( optimizer='adam',
        loss='categorical_crossentropy',
        metrics=['accuracy'])
#create callbacks
checkpoint = ModelCheckpoint("vgg16_model.weights", monitor='val_accuracy', verbose=1, save_best_only=True, mode='max', save_weights_only=True)
early_stopping = EarlyStopping(patience=10, restore_best_weights=True)
callbacks_list = [checkpoint, early_stopping]
#fit model
history = model.fit_generator(generator=training_generator, validation_data=validation_generator, epochs=100, callbacks=callbacks_list)
# summarize
model.summary()

#change path to save mdoel
model.save(r"C:\Users\Study\Desktop\DataGenerator\VGG16\vgg16_model")

test_results = model.evaluate_generator(generator=testing_generator)

print("Evaluation:")
print("loss: ", test_results[0])
print("accuracy: ", test_results[1])
