import os
import json
from  argparse import ArgumentParser

import tensorflow as tf
import mnist

import numpy as np

from my_classes import DataGenerator
from tensorflow.keras.models import Sequential
from sklearn.model_selection import train_test_split

# Parameters
params1 = {'dim': (180,320),
          'batch_size': 16,
          'n_classes': 7,
          'n_channels': 1,
          'shuffle': True}

params3 = {'dim': (180,320),
          'batch_size': 16,
          'n_classes': 7,
          'n_channels': 3,
          'shuffle': True}

# Datasets
partition = { 'train': [], "validation": [], "test": []}
labelsload = np.load(r'C:\Users\Study\Desktop\data_1channel\labels_multi.npy',allow_pickle='TRUE').item()
labels = []
labels.extend(labelsload.keys())

X_train, X_test = train_test_split(labels, test_size=.2, random_state=42)
X_train, X_val =train_test_split(X_train, test_size=.2, random_state=42)


partition["train"]= X_train
partition["validation"]= X_val
partition["test"]= X_test


# Generators
training_generator = DataGenerator(partition['train'], labelsload, **params1)
validation_generator = DataGenerator(partition['validation'], labelsload, **params1)
testing_generator = DataGenerator(partition['test'], labelsload, **params1)

# Design model
from tensorflow import Tensor
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.layers import BatchNormalization, Conv2D, Add, ReLU, AveragePooling2D, Dense, Flatten, Input
from tensorflow.keras.models import Model
import pandas as pd
import numpy as np
from numpy import asarray
import sklearn as sk
from sklearn.model_selection import train_test_split
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping

per_worker_batch_size = 64

parser = ArgumentParser()
parser.add_argument("--config", required=True)
args = parser.parse_args()

with open(args.config) as conf:    
    tf_config = json.load(conf)
    os.environ['TF_CONFIG'] = json.dumps(tf_config)

num_workers = len(tf_config['cluster']['worker'])

strategy = tf.distribute.MultiWorkerMirroredStrategy()

global_batch_size = per_worker_batch_size * num_workers
multi_worker_dataset = mnist.mnist_dataset(global_batch_size)

with strategy.scope():
  def relu_bn(inputs: Tensor) -> Tensor:
        relu = ReLU()(inputs)
        bn = BatchNormalization()(relu)
        return bn

  def residual_block(x: Tensor, downsample: bool, filters: int, kernel_size: int = 3) -> Tensor:
      y = Conv2D(kernel_size=kernel_size,
              strides= (1 if not downsample else 2),
              filters=filters,
              padding="same")(x)
      y = relu_bn(y)
      y = Conv2D(kernel_size=kernel_size,
              strides=1,
              filters=filters,
              padding="same")(y)

      if downsample:
          x = Conv2D(kernel_size=1,
                  strides=2,
                  filters=filters,
                  padding="same")(x)
      out = Add()([x, y])
      out = relu_bn(out)
      return out
      
  inputs = Input(shape=(180, 320, 1))
  num_filters = 64

  t = BatchNormalization()(inputs)
  t = Conv2D(kernel_size=3,
          strides=1,
          filters=num_filters,
          padding="same")(t)
  t = relu_bn(t)

  num_blocks_list = [2, 5, 5, 2]
  for i in range(len(num_blocks_list)):
      num_blocks = num_blocks_list[i]
      for j in range(num_blocks):
          t = residual_block(t, downsample=(j==0 and i!=0), filters=num_filters)
      num_filters *= 2

  t = AveragePooling2D(4)(t)
  t = Flatten()(t)
  outputs = Dense(7, activation='softmax')(t)



  model = Model(inputs, outputs)
  model.compile(
      optimizer='adam',
      loss='categorical_crossentropy',
      metrics=['accuracy']
  )


checkpoint = ModelCheckpoint("resnet_model.weights", monitor='val_accuracy', verbose=1, save_best_only=True, mode='max', save_weights_only=True)
early_stopping = EarlyStopping(patience=5, restore_best_weights=True)
callbacks_list = [checkpoint, early_stopping]
history = model.fit_generator(generator=training_generator,
                    validation_data=validation_generator, epochs=100, callbacks=callbacks_list)

results = model.evaluate_generator(generator=testing_generator)
print(results[0])
print(results[1])